﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;

/// <summary>
/// Animator augment is a supplementary script to AugmentedImageVisualizer.cs
/// Controls **Animator** behavior when attached to an augment.
/// Will cycle through an array of animator controllers and show the appropriate one depending on the image tracked.
/// </summary>

public class AnimatorAugment : MonoBehaviour {
	[SerializeField] private RuntimeAnimatorController[] _controllers; //array of animator controllers
	public AugmentedImage _image; //reference to the augmented image tracked by AugmentedImageVisualizer.cs
	private Animator _animator;

	void Start () {
		//get references to other components
		_image = GetComponent<AugmentedImageVisualizer>().Image;
		_animator = GetComponent<Animator> ();

		//set the animator controller
		_animator.runtimeAnimatorController = _controllers[_image.DatabaseIndex];
		
	}

	void Update () {
		//check if the image has changed
		if (_image != GetComponent<AugmentedImageVisualizer> ().Image) {
			Debug.Log ("Image has changed!");
		}		
	}
}
