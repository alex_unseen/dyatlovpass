﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;

/// <summary>
/// Augmented image controller is a script that is meant to be attached onto the Controller prefab
/// which is present in any AR-enabled scene. Its purpose is to recognize, keep track of, and
/// remove augmented image visualizations. Will pick up Augmented images that are in the config's
/// current database and call the appropriate AugmentedVisualizer that has been set in the
/// _ImageVisualizer field.
/// </summary>

public class AugmentedImageController : MonoBehaviour {

	//reference to the visualizer that is used throughout the scene
	//TODO: Figure out how to use multiple visualizers in one scene
	[SerializeField] private AugmentedImageVisualizer _ImageVisualizer;

	private readonly Dictionary<int, AugmentedImageVisualizer> _visualizers =
		new Dictionary<int, AugmentedImageVisualizer>();

	private readonly List<AugmentedImage> _images = new List<AugmentedImage> ();


	void Update () {
		if (Session.Status != SessionStatus.Tracking) {
			return; //return if not currently tracking anything
		}

		Session.GetTrackables (_images, TrackableQueryFilter.Updated);
		VisualizeTrackables();
		
	}

	//this method runs through the augmented images in the current DB and activates the visualizers if they're being tracked
	//or removes the visualizers if they're not longer being tracked.
	private void VisualizeTrackables(){
		foreach (var image in _images) {
			var visualizer = GetVisualizer (image);

			//if an image is being tracked and the visualizer is null, add it to the dict
			if (image.TrackingState == TrackingState.Tracking && visualizer == null) {
				AddVisualizer (image);
			} else if(image.TrackingState == TrackingState.Stopped && visualizer != null){ //else remove it from the dict
				RemoveVisualizer (image, visualizer);
			}
		}
	}

	//remove a visualizer associated with an index in the database
	//destroy the associated gameobject
	private void RemoveVisualizer(AugmentedImage image, AugmentedImageVisualizer visualizer){
		_visualizers.Remove (image.DatabaseIndex);
		Destroy (visualizer.gameObject);
	}

	//TODO: remember what the fuck this method does LMAO
	private void AddVisualizer(AugmentedImage image){
		var anchor = image.CreateAnchor (image.CenterPose);
		var visualizer = Instantiate (_ImageVisualizer, anchor.transform);
		visualizer.Image = image;
		_visualizers.Add (image.DatabaseIndex, visualizer);
	}

	private AugmentedImageVisualizer GetVisualizer(AugmentedImage image){
		AugmentedImageVisualizer visualizer;
		_visualizers.TryGetValue (image.DatabaseIndex, out visualizer);
		return visualizer;		
	}

}
