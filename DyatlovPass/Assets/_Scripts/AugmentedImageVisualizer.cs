﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;

/// <summary>
/// Augmented image visualizer is a script meant to be attached to all objects
/// that will appear as a result of an augmented image being tracked.
/// Will take care of which image they're tied to as well as their position/scale/rotation.
/// </summary>

public class AugmentedImageVisualizer : MonoBehaviour {

	public AugmentedImage Image; //the image this visualizer is tied to

	public float scaleX;
	public float scaleZ;
	public float scaleY;

	void Update () {
		if (Image == null || Image.TrackingState != TrackingState.Tracking) {
			return;
		}

		transform.localScale = new Vector3 (Image.ExtentX * scaleX, Image.ExtentZ * scaleZ, scaleY); 
		
	}
}
