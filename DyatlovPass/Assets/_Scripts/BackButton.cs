﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Simple script to allow a UI button to go back to the main menu.
/// Set the name of the main menu scene in the editor.
/// </summary>

public class BackButton : MonoBehaviour {

	public string sceneName;

	public void SceneTransition(){
		SceneManager.LoadScene (sceneName);
	}
}
