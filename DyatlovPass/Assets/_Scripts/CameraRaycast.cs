﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A script to be attached to the main camera of each scene.
/// Sets up touch controls via Unity raycasting.
/// On touch, fire a ray straight from the camera and if
/// it hits an object with the tag 'Interactible',
/// then it sends an OnTap() message to trigger the objects behavior.
/// </summary>

public class CameraRaycast : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {

		//Loop through all existing touch gestures
		for (int i = 0; i < Input.touchCount; i++) {
			if (Input.GetTouch (i).phase == TouchPhase.Began) {
				//make a ray from the touch coordinates
				Ray _ray = Camera.main.ScreenPointToRay(Input.GetTouch(i).position);
				//define a container variable to store ray hit information
				RaycastHit _hit;
				if (Physics.Raycast (_ray, out _hit)) {
					if (_hit.collider.gameObject.tag == "Interactible") {
						//if tag is interactible, send the OnTap() message
						_hit.collider.gameObject.SendMessage("OnTap");
					}
				}
			}
		}	
	}
}
