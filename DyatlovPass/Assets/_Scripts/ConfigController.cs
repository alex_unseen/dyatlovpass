﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;

/// <summary>
/// Script to be attached to the ARCore device present in every AR-enabled scene.
/// Will store a reference to the config for the AR session, as well as a reference to
/// the appropriate image database for this scene. On awake, this script will set
/// the config's db to the one meant for this scene.
/// </summary>

public class ConfigController : MonoBehaviour {

	public ARCoreSessionConfig _config; //config reference
	public AugmentedImageDatabase _db; //imagedb reference

	void Awake(){
		_config.AugmentedImageDatabase = _db; //just set it lol
	}

}
