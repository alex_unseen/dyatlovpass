﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A set of debug controls for testing in play mode.
/// Remove/deactivate this component before building and deploying.
/// Meant to emulate the raycast touch controls found in
/// CameraRaycast.cs
/// </summary>

public class DebugControls : MonoBehaviour {
	// Update is called once per frame
	void Update () {
		if (Input.GetAxis("Fire1") > 0.0f) {
			//make a ray at mouse coordinates
			Ray _ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			//define a container variable for hit info
			RaycastHit _hit;

			if (Physics.Raycast (_ray, out _hit)) {
				Debug.Log (_hit.GetType());
			}
		}
	}
}
