﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interface that enables the game manager to store and expose information about the game.
/// the *'Unlocked' bools represent the user's access to the different app utilities.
/// If the user tries to access any of the app utilities while these bools are false,
/// then they will be sent to a securitycheck scene rather than the utility.
/// 
/// Also contains methods that will trigger the booleans when it is appropriate
/// </summary>

public class GameState : MonoBehaviour {
	//TODO: Change public ot private after debugging
	public bool FaceIDUnlocked = false;
	public bool ImageRepairUnlocked = false;

	//on awake check if there is another game manager alive in the scene
	//if there is, destroy this object.
	void Awake(){
		DontDestroyOnLoad (this);

		if (FindObjectsOfType (GetType ()).Length > 1) {
			Destroy (gameObject);
		}
	}

	//method to get a requested boolean value by name
	public bool GetBoolValue(string boolName){
		//check for common spelling mistakes and return appropriate values
		if (boolName == "FaceID" || boolName == "FaceId") {
			return FaceIDUnlocked;
		} else if (boolName == "ImageRepair" || boolName == "Image Repair") {
			return ImageRepairUnlocked;
		} else {
			return false; //return false for a boolName that doesn't exist
		}
		//TODO: add more cases if needed
	}

	//method to set a requested boolean value to a certain state
	public void SetBoolValue(string boolName, bool targetState){
		//check for common spelling mistakes and set appropriate values
		if (boolName == "FaceID" || boolName == "FaceId") {
			FaceIDUnlocked = targetState;
		} else if (boolName == "ImageRepair" || boolName == "Image Repair") {
			ImageRepairUnlocked = targetState;
		} else {
			return; //return if no bool was found.
		}
		//TODO: add more cases if needed
	}
}
