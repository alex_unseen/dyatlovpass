﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Behavior script for the input cubes in mastermind.
/// Cubes should start on a default material with an index of 0
/// Tapping the cubes should let users cycle through materials by incrementing the index
/// </summary>

public class InputCubeBehavior : MonoBehaviour {

	private Renderer _rend;

	public int materialIndex = 0;
	[SerializeField] private Material[] _mats; //available materials for the cube to cycle through

	void Start () {
		//set the initial material to the first material in the array
		_rend = GetComponent<Renderer> ();
		_rend.material = _mats [materialIndex];		
	}

	//update material as needed
	void Update () {
		_rend.material = _mats [materialIndex];
	}

	//OnTap will trigger when the cube is hit by a ray as per the CameraRaycast.cs script
	public void OnTap(){
		//reset or increment the material index
		if (materialIndex == _mats.Length - 1) {
			materialIndex = 0;
		} else {
			materialIndex++;
		}
	}

	/*void OnMouseDown(){
		Debug.Log ("Input Cube Hit");
		//reset or increment the material index
		if (materialIndex == _mats.Length - 1) {
			materialIndex = 0;
		} else {
			materialIndex++;
		}
	}*/
}
