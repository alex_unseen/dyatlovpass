﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Behavior script for the mastermind minigame in FaceIDCheck scene.
/// Holds references to hint, input, and button objects of the game.
/// Responds appropriately via the Reset, Submit, and GiveFeedback methods.
/// Will ping the Gamestate and set the appropriate bool to true when beat.
/// Will also load the FaceID scene.
/// </summary>

public class MasterMindManager : MonoBehaviour {

	//references to the scene objects for input and hints
	[SerializeField] private GameObject[] _inputObjects;
	[SerializeField] private GameObject[] _hintObjects;

	//materials for hint objects
	[SerializeField] private Material[] _hintMaterials;
	[SerializeField] private Material _transmat;

	//numerical representation of the solution, input, and hints
	[SerializeField] private int[] _solution = new int[6]; //set this in the editor or generate on awake
	[SerializeField] private int[] _input = new int[6];

	GameState _gm;

	//grab the reference to the gamemanager and deactivate the access denied banner
	void Start () {
		_gm = GameObject.Find ("GameManager").GetComponent<GameState> ();
		GameObject.Find ("AccessDenied").SetActive (false);
	}

	//triggered when the user hits the submit button
	//TODO: this method can be optimized to have 1 pass through the user's board instead of saving it in memory
	public void Submit(){
		//get the input from the user's board
		for (int i = 0; i < _inputObjects.Length; i++) {
			_input [i] = _inputObjects [i].GetComponent<InputCubeBehavior> ().materialIndex;
		}

		//check if it's the same as the solution
		for (int i = 0; i < _solution.Length; i++) {
			if (_solution [i] != _input [i]) {
				GiveFeedback ();
				return;
			}
		}

		//set gamemanager value to true for faceID
		_gm.SetBoolValue("FaceID", true);
		//immediately load the faceID scene
		//TODO: add some sort of actuall feedback LMAO
		SceneManager.LoadScene("FaceID");

	}

	//triggered when the user hits the reset button
	public void Reset(){
		//set all the input cubes material indices to 0
		for (int i = 0; i < _inputObjects.Length; i++) {
			_inputObjects [i].GetComponent<InputCubeBehavior> ().materialIndex = 0;
		}

		//set the hintquads back to transparent material
		for (int i = 0; i < _hintObjects.Length; i++) {
			_hintObjects [i].GetComponent<Renderer> ().material = _transmat;
		}
	}

	//triggered when the user makes a mistake on submission
	public void GiveFeedback(){
		
		for (int i = 0; i < _solution.Length; i++) {
			//Case 1: check for correct color AND correct position
			//Case 2: check only for correct color
			//Case 3: incorrect color AND position (i.e. the color doesn't exist in the solution)
			if (_input [i] == _solution [i]) {
				_hintObjects [i].GetComponent<Renderer> ().material = _hintMaterials [0];
			} else if (System.Array.Exists (_solution, input => input == _input [i]) &&
			           _input [i] != _solution [i]) {
				_hintObjects [i].GetComponent<Renderer> ().material = _hintMaterials [1];
			} else {
				_hintObjects [i].GetComponent<Renderer> ().material = _hintMaterials [2];
			}
		}
		return;
	}
}
