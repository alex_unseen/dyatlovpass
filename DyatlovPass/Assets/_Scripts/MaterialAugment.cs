﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;

/// <summary>
/// Material augment is a supplementary script to AugmentedImageVisualizer.cs
/// Controls **Material** behavior when attached to an augment.
/// Will cycle through an array of materials and show the appropriate one depending on the image tracked.
/// </summary>

public class MaterialAugment : MonoBehaviour {
	[SerializeField] private Material[] _materials; //array of materials
	public AugmentedImage _image; //reference to the augmented image tracked by AugmentedImageVisualizer.cs
	private Renderer _renderer;

	void Start () {
		//get references from other components
		_image = GetComponent<AugmentedImageVisualizer> ().Image;
		_renderer = GetComponent<Renderer> ();	
	}

	void Update () {
		//check if the image has changed
		if (_image != GetComponent<AugmentedImageVisualizer> ().Image) {
			Debug.Log ("Image has changed!");
		}
			
		//set the material
		_renderer.material = _materials[_image.DatabaseIndex];		
	}
}
