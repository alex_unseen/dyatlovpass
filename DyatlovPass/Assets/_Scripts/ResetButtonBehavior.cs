﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Reset button behavior for mastermind.
/// OnTap will just call the reset method from the parent MasterMindManager object.
/// </summary>

public class ResetButtonBehavior : MonoBehaviour {
	public void OnTap(){
		GetComponentInParent<MasterMindManager> ().Reset ();
	}

	void OnMouseDown(){
		GetComponentInParent<MasterMindManager> ().Reset ();
		Debug.Log("Reset Button Hit, OnMouseDown");
	}
}
