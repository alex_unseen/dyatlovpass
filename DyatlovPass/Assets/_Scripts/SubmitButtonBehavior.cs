﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Submit button behavior for mastermind.
/// OnTap will just call the submit method from the parent MasterMindManager object.
/// </summary>

public class SubmitButtonBehavior : MonoBehaviour {

	public void OnTap(){
		GetComponentInParent<MasterMindManager> ().Submit ();
	}

	void OnMouseDown(){
		GetComponentInParent<MasterMindManager> ().Submit ();
		Debug.Log("Submit Button Hit, OnMouseDown");
	}
}
