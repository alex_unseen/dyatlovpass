﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Script to be attached to a UI element to make it pulse.
/// e.g. the access denied banner in the check scenes.
/// </summary>

public class UIPulse : MonoBehaviour {

	//pulse speed. tweak this in the editor
	public float pulseSpeed = 1.0f;

	//placeholder var for color
	public Color c;

	void Start(){
		c = GetComponent<Image> ().material.color;
		c.a = 1.0f;
		GetComponent<Image> ().material.color = c;
	}

	//TODO: this really should be a coroutine...
	void Update () {
		//Get Color and store it in c
		c = GetComponent<Image> ().material.color;
		//check if alpha is either 0 or 1 to change the sign od dα
		if (c.a >= 1 || c.a <= 0) {
			pulseSpeed *= -1;
		}
		//add delta alpha
		c.a += pulseSpeed*Time.deltaTime;

		//set the material color to c
		GetComponent<Image> ().material.color = c;
	}
}
