﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Behavior script for any UI button that leads to an app utility (faceID, ImageRepair etc.)
/// Given a boolean name to check for, the script will ping the GameManager
/// for a boolean value that will determine which scene the button will load.
/// If the boolean is true, then the button will load the utility as expecte.
/// Else, it will load a security check scene for the user to play a minigame.
/// </summary>

public class UtilityButton : MonoBehaviour {
	//reference to the game manager
	public GameState _gm;
	//boolean name to look for
	public string boolName;
	//scene names to transition to based on the bool check
	public string successScene;
	public string failureScene;

	//method to load the next scene by name
	public void SceneTransition(){
		if (_gm.GetBoolValue (boolName)) {
			SceneManager.LoadScene (successScene);
		} else {
			SceneManager.LoadScene (failureScene);
		}
	}
}
